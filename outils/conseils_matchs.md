# Improsture -- Conseils Match
 
Assurez-vous d'avoir bien compris le décorum, le rôle du MJ, celui de l'arbitre, etc.

Quelques conseils:

 1. Définissez un capitaine (préférablement quelqu'un qui a déjà joué un match ou qui connaît bien les règles).
 1. Avant le spectacle, mélangez vous à l'autre équipe pour l'entraînement, en pensant notamment à définir une convention pour venir ensemble sur scène. Ex : si je fais ce geste, je t'invite à venir jouer avec moi
 1. Bien écouter les consignes sur les catégories pour être sur d'en comprendre l'enjeux. Ne pas hésitez pas poser des questions.
 1. Caucus mixte: qui y va, quelles charactéristiques du personnage, éventuellement relation ou lieu. Caucus comparée: qu'est-ce qu'on va jouer.
 1. Assurez-vous de bien traiter le thème. Les contraintes de style sont importantes, mais secondaires.
 1. Attention aux contraintes de nombre de joueurs ! Si c'est une comparée, n'entrez jamais à plus que le nombre max. Si c'est une mixte, n'entrez jamais à plus que la moitié du nombre max.
 1. Regardez l'arbitre pour le temps qu'il reste. Ce n'est pas un spectacle, les timings doivent être exacts et les chutes placées exactement à la fin.
 1. Si vous voulez entrer à 2 dans une mixte, ne rentrez jamais avec quelqu'un de votre équipe, mais allez chercher un joueur de l'autre équipe. Si vous entrez seul, vérifiez quand même que votre équipe n'a pas mis 5 joueurs contre 1 seul en face.
 1. Quand l'arbitre siffle le début d'une impro mixte, un joueur de chaque équipe doit déjà se trouver dans la patinoire ! Si vous entrez dans la patinoire après le sifflement, c'est un retard de jeu.
 1. Quand vous jouez une comparée, 2 choses importantes:
	 - NE PARLEZ PAS sur  le banc quand l'autre équipe joue. Pas des gestes, pas de regards, rien. Même demander l'heure est une faute.
	 - Essayez de laisser l'équipe d'en face jouer en premier. Statistiquement, le public vote plutôt pour la 2ème impro.
 1. Vous devez TOUJOURS toucher la patinoire si vous faites quoi que ce soit (même un son pour une impro).
 1. Vous devez TOUJOURS porter votre maillot, mais vous pouvez jouer avec (en faire un foulard, etc.).
 1. Si vous êtes dans la patinoire et que quelqu'un commence une nouvelle scène où vous ne jouez pas, vous devez rester dans la patinoire, idéalement planqué contre une paroi.
 1. Ne suivez pas ces conseils au détriment de l'impro ! Mieux vaut faire une belle impro, se prendre une faute et gagner la faveur du public que de faire une impro nulle à cause de ces contraintes.
Bonne chance !

