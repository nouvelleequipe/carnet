# Réunion équipe de coordination du 4 juin
Présent·e·s : Chloé, Yann, Guillaume, Maïeul, et Laurent à la fin

## Relation PHIL

- Laurent délégué à la réunion de PHI dans tous les cas
- Tous les autres peuvent aller
Problématiques :
	- calendrier
	- sous
- Proposition à faire lors de l'AG : une instance "PHI" regroupant un représentant du PIP, un de l'Heidi et un de l'Improsture qui se réunion 2 fois semestre pour gérer les dates / le budget. Pour nous en raison de relations avec Zélig, il faudrait que d'ici fin août le calendrier soit fixé.


## Question des relations avec l'Heidi

Une personne chargé de planifier les entrainements avec eux


## Finances

On passe


## Propositions de spectacles pour le semestre d'automne 2019

- Un long format à la fin du semestre
	- Appel à contribution pour concept (délai de rendu 1er octobre)
	- Vote sur le concept : (délai de vote 7 octobre)
	- Spectacle : début décembre / fin novembre
- Courts formats :
	- On cherche d'abord à intégrer la ligue découverte Improsuisse afin :
	- De faciliter l'orga
	- De rencontrer d'autres équipes
	- Voir aussi du côté de Zélig
	- Sinon, on organise nous même

## Calendrier d’entraînement :

Demander d'ici 15 jours aux membres actuelles de choisir entre plusieurs modalités (condorcet):

- Jour fixe
- Alternance
- Par mois
- Semaine par semaine

En fonction du résultat du vote on fait les votes qui en découle

## Recrutement de nouveaux membres

- Pub IA
- Présence aux journées de rentrée
	- Yann + Guillaume (+ Chloé) pourrait y aller en tant que représentant Improsture
- Pub réseaux sociaux: Chloé / Guillaume / Yann

## Communication interne et externe

- Réseaux sociaux : Guillaume, Laurent, Yann, Chloé
- Web
	- Techniques : Laurent ou Maïeul + formation de Chloé
	- Contenu : Chloé / Yann / Maïeul

## Répartition des rôles entre membres de l'équipe de coordination

- Maïeul :
	- planification des entraînements, entraînements communs avec l'Heidi
	- contact ligue découverte Improsuisse
	- relation salles (Zélig)
- Yann / Guillaume + d'autres membres de l'équipe : stand de rentrée
- Guillaume / Chloé / Yann / Laurent : réseaux sociaux
- Laurent : finance + relation PHI (avec Maïeul)




