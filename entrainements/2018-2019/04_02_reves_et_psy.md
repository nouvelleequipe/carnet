# Entraînement du 4 février 2019: **Rêves et psys**

Entraîneur : Iann
## But de l'entraînement

Utiliser les potentialités du rêve et de son récit en impro.

## Échauffements
- Le proverbe: On choisit un proverbe simple. Dans l'ordre du cercle, chacun·e dit l'un des mots du proverbe. _Difficulté 1_: le même exercice en se déplaçant dans l'espace (l'ordre de passage est conservé, ce qui implique de se souvenir à côté de qui on était dans le cercle). _Difficulté 2_: Même exercice + Lorsque l'on croise la personne placée à notre gauche dans le cercle, on lui sert la main. _Difficulté 3_: Même exercice que le précédent + Lorsque l'on croise la personne située à la gauche de notre voisin·e de gauche dans le cercle, on réoriente son déplacement.
- Compter jusqu'à 8: Dans l'ordre du cercle, on compte jusqu'à 8. A chaque fois qu'une personne hésite ou se trompe, elle court autour du cercle dans le sens des aiguilles d'une montre. _Difficulté 1_: On indique le sens du décompte en se tapant sur l'épaule droite ou gauche. _Difficulté 2_: A la place de 3 et de 7, on place ses deux bras parallèles devant soi. Le sens de la main supérieure donne le sens de la poursuite du décompte.
- L'objet au centre: L'entraîneur/euse choisit un objet, censé se trouver au centre du cercle des improvisteurs/trices. Sans ordre particulier, les personnes créent des phrases se rapportant à cet objet et commençant successivement par "Je vois...", "Tu es...", "Vous êtes..." (ton emphatique) et "Je suis..." (les personnes s'identifient à l'objet en question).
- Epuisez le lieu!: Le cabinet du psy: Les personnes énumèrent des objets pouvant se trouver dans le cabinet d'un psy. Les répétitions sont permises, ainsi que les amplifications ("un mouchoir", "des mouchoirs", "une pile de mouchoirs utilisés").

## Impros
1. **"La nuit dernière, j'ai fait un rêve incroyable!"**
Groupes de deux.Un·e rêveur/euse et sa conscience. Le/la réveur/euse est allongé. La conscience lui raconte son rêve à l'oreille. Tour à tour, chaque rêveur/euse raconte le rêve que lui a suggéré sa conscience en commençant par "La nuit dernière, j'ai fait un rêve étrange..." et en terminant par "... ensuite, je me suis réveillé·e"

2. **Cauchemars en cascade.**
Une personne est le/la rêveur/euse. Elle choisit une action douce. Les autres entrent en service pour faire dégénérer la scène en cauchemar. Le/la rêveur/rêveuse choisit d'interrompre son rêve en claquant des mains. Elle recommence une action douce. Les autres, en service, font à nouveau dégénérer l'action en cauchemar.