# Adresse publique et monologue

Entraineur : Maïeul

## Echauffement

- course
- voix

## Mot lancés thématiques

Autour d'une émotion. Pas nécessairement le thème de l'émotion, mais des élèments qui peut provoquer cette émotion ou que cette émotion peut provoquer

## Entrainement en binome

Se répartir dans la salle. Voir un objet banal. Et se mettre à "déblatterer" sur cet objet. Qu'est ce que l'objet pourrait évoquer comme souvenir / émotion / sentiment.

## Scène

Scène avec impulse lieu / relation. A n'importr quel moment le public peut dire "je veux une adresse publique"
