# Habiter un lieu

Entraineur : Maïeul

## Echauffement

- course
- écoute : deambuler dans la pièce, des actions à faire synchro sur certaines paroles, s'arreter, poser des questions sur le lieu

## Etape 1 : le lieu construit

A tour de role, entrée dans un lieu et y "poser" un objet. La personne qui vient après doit reprendre l'objet posée, et en ajouter un autre. Donc plus on arrive tard, plus on a d'objet à reprendre.

## Interlude

Jeu d'energie type watcha / samourai
## Etape 2 : le lieu posé

Goaler "de nazi" avec un lieu et une relation posé en 3 répliques.

## Etape 3 : le lieu habité / vécu

Petites impros de 3 minutes. Les 30 premières seconde doivent poser le lieu.
