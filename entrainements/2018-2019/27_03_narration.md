# Narration et Mécanique de l'Impro

Entraîneur : Laurent

## Echauffement

1. Echauffement en cercle
1. Clap
1. Balles de couleurs
1. Clap lent + balles
1. Pan

## Plateforme

### Comment démarrer une impro?

Faire du théatre, c'est comme faire une course de ski. Il y a des passages obligés et des changements de direction, mais le chemin est déjà balisé. 

Faire de l'impro, c'est comme skier hors-piste. Le but est de trouver la meilleure trace possible. Pour ça il nous faut 4 choses: une piste, des skis, savoir skier et aller quelque part.

La piste est la plateforme: où, quand, qui, relation et quoi (l'action). 

Comment trouver une bonne plateforme ? Partir sur une piste bien damée, mais avec de la pente. En d'autres termes, prendre une plateforme simple, situation ordinaire, personnages ordinaires, etc. auquel le public s'identifie facilement. On rendra cette plateforme intéressante en changeant un (ou plusieurs -- mais pas trop) élément(s) pour le placer à lisière du cercle d'attentes.

Par exemple: une mère et son fils qui font des courses dans un supermarché. On prend un des éléments de la plateforme (où, quand, qui, relation, quoi) et on le change:

* Si on change où : l'impro se passe dans une décharge municipale.
* Si on change quand : ils sont hyper pressés.
* Si on change qui : les enfants ont 40 ans.
* Si on change la relation : les enfants sont abusés.
* Si on change quoi : ils esaient de voler des provisions.

Ca fonctionne pour tout et on obtient une plateforme relativement claire, mais intéressante, la piste a de la pente.

### Exercice Pratique

Impro par groupes de 2 + 2. Je vous donne une plateforme. A vous de changer un élément qui va la rendre intéressante. 2 personnes rentrent en lead, les 2 autres écoutent le caucus, ne proposent rien, mais rentrent en service si besoin pour clarifier des éléments manquants.

Plateformes:

* Chirurgien et son assistante opèrent un patient
* Patron dicte une lettre à sa secrétaire
* Un homme se confesse à un prêtre
* Deux retraités discutent du temps
* Visite chez le coiffeur
* ...

## Un petit mot sur le personnage et la caractérisation

Dans la métaphore, cela correspondrait à chausser nos skis. Pas le sujet de cet entraînement.

## Mécanique de l'impro

### Dynamique de base

Revenons encore à notre métaphore. Maintenant que nous savons faire apparaître la piste, chausser nos skis et qu'on sait (dans une moindre mesure) skier, il s'agit de naviguer dans la piste. C'est là qu'interviennent les concepts de "oui, et...", "oui, mais...", etc.

### Exercices Pratiques

2 par 2 pendant environ 1 minute chaque fois. 

* A commence à faire des propositions à B. B n'a le droit de dire que "non". On inverse.
* A fait une proposition à B. B enchaîne avec "oui, et...". A continue, etc.
* A fait une proposition à B. B enchaîne avec "oui, mais...". A continue avec "oui, et...", etc.
* Idem que précédemment, mais A et B ont chacun le droit de dire une fois "non, mais...".

### Progression et directions

Si on revient à notre métaphore de ski, quand on dit "oui, et", on prend de la vitesse. On peut aussi dire "oui, mais", càd on continue d'avancer, mais on propose une autre direction plus intéressante. On peut aussi dire "non", ce qui revient à freiner dans la pente, voire à refuser la pente. Enfin, on peut aussi dire "non, mais", càd freiner pour changer de direction car on sent qu'on va droit dans un ravin. 

Attention : dire "non" ce n'est pas nier la pente. Nier ce qui se passe sur scène c'est ça qui est interdit en impro (refus).

## Construction d'histoires

Oublions un moment le schéma quinaire dont on nous parle à chaque entraînement sur la construction et tentons une approche différente, par le bas (*bottom-up* au lieu de *top-down*).

L'atome d'une histoire est ce qu'on appelle le *beat*. Un beat pose une intention pendant un échange sous forme d'action-réaction. Par exemple, faire la vaisselle, la cuisine, lire le journal, mettre une baffe à quelqu'un, déclarer sa flamme, etc. Plusieurs beats vont ensuite s'agglomérer pour former une scène, une molécule de l'histoire.

Comment caractériser une scène ? Parlons de *story values* (valeurs d'histoire). Une story value est un état de situation auquel est associé un état opposé. Un personnage riche devient pauvre, un monde dévasté est renouvellé, etc. On peut avoir plusieures story values par scène et elles peuvent évoluer. Une scène est donc un ensemble de beats qui aboutissent à l'inversion d'une story value. Le personnage, la situation, etc. change. 

Les scènes vont ensuite, ensemble, produire des séquences, qui se caractérisent par un changement d'une valeur d'histoire majeure. Les séquences vont former des actes, qui se caractérisent par un changement d'objectif du personnage principal. Finalement, les actes, ensemble, font une histoire.

### Exercices & Impro

#### Scènes

Impro avec 2 personnes en lead, services autorisés. Environ 1 minute. Vous partez d'un impulse que je vous donne qui sera de la forme suivante : à la fin de cette scène, je veux que X (story value) ait changé. A vous de décider comment, et vous n'avez pas besoin de poser la plateforme dans ses moindres détails. Mettez plusieurs beats ensemble et construisez une scène. Vous pouvez ajouter d'autres story values, mais essayez de ne changer que celle qu'on vous a demandé.

Impulses:

* Pauvre -> riche
* Haine -> amour
* Le monde entier devient malheureux
* Personnage perd son père
* ...

#### Séquences

Idem que précédemment, mais pour 2-3 minutes avec une séquence de scènes qui doivent amener à un changement plus important de story value.

Impusles:

* Monde dévasté
* Personnage perd sa virginité
* X décide de tuer Y
* ...

#### Actes & Histoire

Impro relativement longue de 5-10 minutes. L'objectif est de poser une plateforme rapidement, et de construire une histoire, ou au minimum un acte. Pour ce faire, on emploiera les briques de constructions dont on a parlé précédemment.

## Références

* Improvisation for Storytellers, Keith Johnstone
* Improvisation and the Theatre, Keith Johnstone
* Story, Robert McKee
* Screenplay, Syd Field
* Ex Machina, Arnaud Pierre