# Émotions et sentiments

Entraineur:Maïeul

## Rappel théorique

- Avec les relations, les émotions font partie des élèments qui font que le public se prend au jeu de regarder le spectacle
- **Emotions** et **sentiments** sont deux choses distinctes:
	- **Emotions** : réactions spontanées à une situations, qui se manifeste physiquement et/ou psychologiquement
	- **Sentiments** état affectif durable qui évolue avec le temps
- En pratique du coup, notre personnage pourra avoir des sentiments (ex: ielle est amoureuse) qui se manifesteront par des emotions dans un contexte (ex. si la personne dit oui, ielle est joyeuse, si la personne dit non, ielle est triste)
- Comme le public ne peut pas accéder à la tête du personnage, il n'a accès directement qu'aux émotions, et pas au sentiments (qu'il peut déduire des émotions dans un contexte, mais pas évident)
- Les ruptures d'émotions apportent de l'interet à l'histoire

Il  y  plusieurs théories sur le nombre d'émotions de base, etc. On va prendre celle de
http://espacelibre.qc.ca/articles/saison-2014-2015/liste-des-emotions


## SENTIMENTS D'AMOUR
1. Amour
2. Admiration
3. Amitié
4. Affection
5. Sympathie
6. Charme
7. Bonté
8. Tendresse
9. Désir
10. Convoitise
11. Compassion
12. Sentiment
13. Excitation
14. Enchantement
15. Séduction
16. Passion
17. Affection
18. Engouement
19. Chaleur

##SENTIMENTS DE JOIE
20. Joie
21. Plaisir
22. Satisfaction
23. Délectation
24. Entrain
25. Heureux
26. Ardeur
27. Euphorie
28. Satisfaction
29. Soulagé
30. Gaieté
31. Jouissance
32. Jubilation
33. Sensation
34. Amusant
35. Extase
36. Optimiste
37. Triomphe
38. Allégresse
39. Contentement
40. Fier
41. Ferveur
42. Jovial
43. Humour
44. Enchanté
45. Excitation

##SENTIMENTS DE COLÈRE
46. Colère
47. Agressivité
49. Agitation
50. Amertume
51. Couroux
52. Cruauté
53. Dégoût
54. Mécontentement
55. Vengeance
56. Frustration
57. Maussade
58. Haine
59. Furie
60. Tourment
61. Jalousie
62. Bougon
63. Rage
64. Irritation
65. Ressentiment
66. Exaspération
67. Outrage
68. Mépris
69. Férocité
70. Répugnance
71. Révulsion
72. Destruction
73. Rancune
74. Aversion

##SENTIMENTS DE TRISTESSE
75. Tristesse
76. Négligence
77. Cafard
78. Chagrin
79. Désespoir
80. Pitié
81. Égarement
82. Malheur
83. Anxiété
84. Insécurité
85. Défaire
86. Mécontentement
88. Douleur
89. Mélancolie
90. Abandon
91. Dépression
92. Déplaisir
93. Souffrance
94. Solitude
95. Désarroi
96. Accablement
97. Irrémédiable
99. Rejet
100. Seul(e)
101. Découragement
102. Désapointement
103. Misère
104. Agonie

##SENTIMENTS DE CRAINTE
105. Crainte
106. Appréhension
107. Méfiance
108. Panique
109. Hystérie
110. Nervosité
111. Effroi
112. Tension
113. Inquiétude
114. Secousse
115. Souci
116. Horreur
117. Saut d'humeur
118. Anxiété
119. Terreur
120. Écrasement
121. Détresse

##SENTIMENTS DE HONTE
122. Honte
123. Remord
124. Humiliation
125. Culpabilité
126. Regret
127. Embarras
128. Invalidation
129. Confusion
130. Insulte
131. Pénitence
132. Mortification

## AUTRES SENTIMENTS IMPORTANTS

133. Intérêt
134. Irritation
135. Timidité
136. Prudence
137. Audace
138. Excitation
139. Épuisement
140. Fragilité
141. Regret
142. Bravoure
143. Curiosité
144. Insatisfaction
145. Réserve
146. Suspicion
147. Courage
148. Intrigue
149. Répugnace
150. Pudeur
151. sursaut
152. Détermination
153. Surprise
154. Ahurissement
155. Feinte
156. Étonnement
157. Compétent
158. Stupéfaction
159. Intimidation
160. Réticence
161. Impuissant
162. Bougeotte
163. Indécision
164. Domination
165. Culpabilité
166. Soumission
167. Doute
168. Apathie
169. Ennuie
170. Impatience
171. Indifférence
172. Amorphe


## Échauffement

- Course, on rencontre des gens ou des êtres qui nous provoquent des émotions
- marcher dans l'espace, et faire des émotions entre 1 et 10 (Maïeul lance)
- jeu d'écoute : passage de clap, chemin de prénom, etc

Une personne répte en boucle le nom d'une autre personne situé dans les bancs, en hauteur, à une certaine distance. Cela nourrit des émotions intérieurement. Quand elle juge bon, elle exprime ses émotions et ses pensées en s'adressant à la personne (note : il s'agit d'impro, on ne dit pas ce qu'on pense réellement de la personne, la personne est juste un acteur).

## Impros
2 personnes arrivent. Chacun à un désir / sentiment secret vis à vis de l'autre (sentiments qui est donné par quelqu'un). Avec ce sentiment, il doit trouver une émotion correspondant. Si une personne ne voit pas l'émotion declenché par un propos important, alors il peut interrompre l'impro.

Impulse :dictionnaire de lieu.
