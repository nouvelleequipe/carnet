# Entraînement du 4 mars 2019: **Personnages**

Entraîneur : Chloé

## Echauffement : 

Tout le monde se met en cercle : 

* Réveil corporel : frotter son corps pour se réveiller
* Chaque personne propose tour à tour un geste que tout le monde imite
* Sa-mou-raï
* Yoga du regard : tout le monde fixe le sol jusqu’à ce que qqn lève la main et les regards se dirigent sur elle/lui. Ensuite, il regarde clairement et lentement qqn d’autre dans les yeux pour échanger le regard. Tout le monde se met alors à fixer la même personne. Et ainsi de suite. 
* Exercice de la secte :  on sort un souffle qui devient peut à peut de plus en plus fort, de manière synchrone

Marcher dans la salle : 
* Yoga du regard marché avec de la musique : ajouter une émotion, une couleur aux interactions. 
* Toujours en marchant (de manière neutre cette fois-ci), qqn propose une émotion qui se propage par le regard chez les autres joueurs. Peu à peu cette émotion touche tout le monde et s’amplifie pour atteindre son maximum. Ensuite elle diminue et on revient à un état neutre. Puis rebelote. 
* Marcher dans l’espace de manière neutre, lorsque l’entraineuse/eur tape des mains, tout le monde doit sauter de manière synchrone. Si iel dit « attention » les gens doivent se regrouper, si iel dit « bonjour » les gens doivent s’accroupir. Les joueurs/euses peuvent aussi taper des mains/dire bonjour/etc.
* Marcher dans l’espace et imiter la démarche de qqn jusqu’à ce que tout le monde ait la même démarche. A ce stade qqn propose autre chose et les gens imitent (give and take). 

Deux par deux : 
* Commencer une phrase par « c’est incroyable que tu me dises ça parce que… » ou « c’est important que tu me dises ça parce que… ». Chaque phrase doit avoir ça. Cet exercice permet de travailler l’impact des phrases sur son propre personnage et la construction de son histoire. Il permet de creuser le pourquoi.

## Impros : 

* Sans paroles, une phrase maximum (de préférence prendre son temps avant de la dire). Il s’agit de construire une ambiance et une relation sans pour autant partir dans le pantomime. Pour ça c’est important de jouer avec une émotion claire et de bien se regarder entre joueurs/euses. 
* Echo: 2 groupes de 2 sont sur scène. A tour de rôle ils discutent de 2 discussions très différentes. Peu à peu, les discussions doivent se faire écho (que ce soit le même mot utilisé, un thème, un geste qui se répète ou une relation qui se répète (exemple des deux personnages qui n’ont pas de tact), etc.).

* Ronde : Chaque joueur/euse joue un personnage. Le joueur A joue une première impro avec le joueur B. Le B joue ensuite avec le C puis le C avec le A. A la fin, tous les joueurs/euses jouent ensemble. Cet exercice demande de la concentration afin d’intégrer tous les personnages et évènements à la fin. Il faut rester sur le fil rouge et préparer la fin un peu avant en réfléchissant à la conclusion


### Conseils : 
* relation claire et colorée posée rapidement
* enjeu
* lieu
* utilisation de l’espace

### Lieux : 
* montagne
* bains thermaux
* ascenseur
* cave
* avion
* champ de tournesol
* vaisseau spatial
* Manifestation 
* ems
* bar
* salle de réunion
* toilettes
* chez le psy
* caféteria
* une chambre d’hôpital
* une salle de théâtre

### Thèmes : 
* le secret de la pyramide
* les sables émouvants
* la plante grimpante
* amour sans r
* 2123
* la maison invisible
* Route de la colline 3
* Le livre maudit
* Le déménagement
* Repas de famille
* Elle ne voulait pas grandir
* Il était encore vivant
* Une toute petite erreur
* C’est moins cher plus tard
* Eh bien c’est raté !
* Dans un monde de fou seuls les fous sont sains d’esprit
* la communication malveillante
* Le cours de cuisine
* Imitation de vitesse
* Elle est belle ta tronçonneuse
* Les poux
* La prophétie auto-réalisatrice
* La vérité qui dérange
* L’herbe est toujours plus verte chez soi
* Randonnée sur un arc en ciel
* Le juste milieu
* Renaissance fatale
* L’orage violet
* Le match de boxe
* L’angoisse de la page blanche
* Ça va se voir
* La photo de classe
* Piles non comprises

https://www.dramaction.qc.ca/fr/ressources/exercices/page/58/


