# « Découvrir l’histoire »
Entraîneur : Yann

Pour construire une impro, on peut arriver avec la dizaine de débuts
d’impro qu’on sait faire, des idées sur comment chacune d’entre elle
devrait se développer et pourrait se terminer. Généralement, si l’impro
se fait avec d’autres improvisatrices/improvisateurs, il y a de grands risques
qu’il faille abandonner tout ce qu’on avait prévu en cours de route (à
moins d’avoir une improvisatrice/un improvisateur docile en face). Tout aussi
intéressant (et moins coûteux en énergie) d’être au présent (Mark Jane),
disponible à l’autre, à l’écoute des émotions que provoquent l’autre ou
l’environnement chez votre personnage. Il n'est possible d’atteindre cet état
d’esprit que quand on ne prévoit rien à l’avance, qu’on se
concentre sur ce que le personnage ressent et qu’on construit au fur et
à mesure de cette découverte.

Question qui guide l'entraînement : qu’est-ce que ça fait à mon personnage ?

## Échauffement

### Échange de regard, puis de clap (d’après Lionel Perrinjaquet)

#### 1) Autour du cercle

D’abord prendre le temps de recevoir le regard et de le donner, puis dès
que contact entre les deux regards a eu lieu, on passe au suivant (=&gt;
bcp plus rapide). 

La personne qui reçoit peut anticiper en regardant déjà en direction de
la personne qui va lui envoyer le regard.

Puis on ajoute : retour à l’expéditeur (si échange de regard paraît plus
long, signifie que l’autre le renvoie ; si échange très long : l’un des
deux doit décider de le renvoyer à qqun d’autre)

#### 2) A l’intérieur du cercle

Une personne regarde par terre, tout le monde la regarde, elle regarde
qqun de précis, tout le monde regarde cette personne.

D’abord prendre le temps d’envoyer et de recevoir le regard : travailler
sur la précision de l’adresse. Puis plus vite. Puis inclure retour à
l’expéditeur. A la fin, on peut poser la question: "qui a perdu le fil mais
a pu le récupérer ?"")

NB: L'échange de clap est en réalité un échange "sonore" de regard. 
Sans échange de regard, échange de clap ne donne rien.

Clap commun : clap commun avec les voisins de gauche et de droite ;
travailler la précision du clap : on répète le clap jusqu’à ce que les
deux clappent exactement en même temps.

Tuyau : bien se regarder et respirer en même temps avant le clap permettent 
aux deux personnes de bien se synchroniser.

### Contaminé·e·s par l’émotion (d’après Adrien Knecht)

Marcher dans l’espace. S’arrêter en même temps–repartir en même temps.

Marcher dans l’espace, expression neutre. MJ touche l'épaule de qqun qui 
décide d’une émotion, d’un son, potentiellement d’une posture
corporelle/démarche qui va avec. Ces éléments sont progressivement
imités par l’ensemble du groupe.

D’abord retour au neutre entre chaque, puis passage d’une émotion à
l’autre.

Marcher dans un lieu :

- dans un hangar vide sur un parquet qui craque
- sur une plage de sable fin, au bord de la mer
- dans le couloir d’un hôpital
- sur une plage de petits galets

### Mother (d’après Adrien Knecht)

« Mother I can feel you under my feet! Mother I can hear your heartbeat!
Oooooooh (clap) oooooooohaaaaaaah »

### Le cercle du tilt (d’après Odile Cantero)

Tous en cercle. Tous sont des voisins d’immeuble et vont informer les
autres de quelque chose susceptible d’affecter la personne informée.

Une personne A va frapper à la « porte » de B. B ouvre. A lui dit une
phrase quelconque (qqch qui pourrait l’affecter) **en regardant B dans
les yeux**. B regarde A, voit quelle émotion sa phrase provoque chez lui
(sans réfléchir) et s’installe dans cette émotion en l’augmentant
progressivement (tilt). B attend d’être bien installé dans son émotion
pour répondre à A en justifiant son émotion.

Partir de l’émotion et la justifier ensuite seulement permet de
découvrir pourquoi le personnage a réagi ainsi et pas de construire
l’émotion pour créer artificiellement un effet.

NB: Peut aussi servir a relancer une impro qui « patine ».

## Débuts d’impros avec émotion

Adapter son émotion à l’émotion de l’autre ou reprendre l’émotion de
l’autre (comme dans "Contaminé·e·s par l'émotion") peut suffire à lancer une
impro ou à colorer une impro.

### Ce que ce lieu me fait (d’après Adrien Knecht)

Deux joueurs A et B. Le public donne un lieu.

A entre et écoute comment il se sent dans ce lieu, s’y déplace,
interagit avec le lieu. B observe A (pas de précipitation : n’est pas
minuté !). Dès qu’il a compris l’état d’esprit dans lequel se trouve A,
il entre avec la même émotion (peut choisir de l’augmenter ou non). En
fonction de ce qu’il ressent, A nomme B (prénom, relation) et les deux
commencent une impro.

### Ce que me fait un objet dans ce lieu (d’après Adrien Knecht)

A entre dans un lieu qu’il choisit lui-même. Dans ce lieu, il trouve un
objet chargé d’une émotion particulière. Objet peut être portable ou
non. A prend le temps de considérer l’objet et de se mettre dans
l’émotion que cet objet suscite en lui.

B observe. Quand il comprend l’émotion, il entre. A ou B précise l’objet
et la relation.

Exercice montre qu’il est inutile de trop construire à l’avance : B
comprendra le plus souvent autrement et l’émotion, le lieu et l’objet
imaginés par A. Tout ce qui est posé (par A ou par B) existe =&gt; l’un
des deux devra lâcher le film qu’il s’était fait pour construire à
partir de ce que l’autre lui propose.

### Impros à partir d’émotions *(d’après Tania du PIP ?)

2 joueurs dos à dos. Chacun se met dans une émotion qu’il choisit. Puis
se retourne. Puis regarde l’autre. Prend le temps de recevoir l’émotion
de l’autre et de voir ce qu’elle fait à son personnage. Impro commence
(services autorisés).
