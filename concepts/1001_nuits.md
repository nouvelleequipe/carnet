# Milles et une nuits

Principe de "récit imbriqués".

La première scène commence de la manière suivante : deux personnes ont un dialogue, et l'un·e demande à l'autre de raconter une histoire (soit parce qu'elle veut entendre une histoire, soit parce que le dialogue appel à témoignage).
En général, l'histoire racontée est celle de la personne qui la raconte "J'étais sur le port de ... quand".
La personne qui raconte l'histoire devient narratrice. Il y a donc une nouvelle scène qui commence, avec un·e narrateur/narratrice et des gens qui jouent l'histoire.

Au sein de l'histoire racontée, le même processus de "je te raconte une histoire" a lieu.

On peut ainsi enchaîner plusieurs niveaux (je pense jusqu'à trois max).

Comme pour toutes les catégories avec narrations, la difficulté est de trouver le bon équilibre entre place du narrateur/ de la narratrice et places des gens qui jouent effectivement l'histoire

Par ailleurs il faut gérer le timing pour bien "clore" les histoires.

Pour donner un peu de coloration, on peut décider de jouer cela dans un univers "oriental" (style Aladin, Ali Baba) mais pas nécessairement.

