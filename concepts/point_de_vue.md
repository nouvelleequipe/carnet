# Concept point de vue
On commence par une courte séquence, un peu forte (meurtre, mariage)

Puis on présente 3 points de vue/vision sur comment on est arrivé à cette séquence.

Ex si un mariage:
- le point de vue de la fiancée
- puis celui du fiancé
- puis celui d'un proche

Pour que cela fonctionne bien, il faut que les protagonistes de l séquence finale se connaissent à la fin de l'histoire, mais pas au début de l'histoire. L'enjeu est de voir comment des relations vont se tisser entre elleux.

