# La boite à souvenir / flash back

Idée : Maïeul

## Intéret

- jouer sur les relations
- jouer sur la construction narrative
- jouer sur les points de vue


## Déroulé

- En début d'impro, 1-2-3 personnes trouvent par hasard (rangement) une photo
	- cette photo est un souvenir.
	- Elle contient les 8 personnages (si nous avons 8 acteurs/actrices), même si seulement 3 personne retrouvent la photo
	- on demande au public le type de scène (ex : photo de classe, photo de plage, photo de vacances, etc.)
	- on "joue" cette photo pour que les gens puissent se la représenter (image figée)
- les gens qui ont découvert la photo commence à en parler, à rappeler les souvenirs "et tu te te souviens quand untel... une telle"
- on bascule ensuite dans "on joue le souvenir".
- puis les autres personnages (les 5 qui restent) retrouve une autre photo, du même groupe
- même processus, cela reenclenche des souvenirs

## Ce qu'il faut jouer

- Comment les personnages se sont rencontrés, comment ils se sont séparés, ou pas
- les personnages ont du vivre un moment fort ensemble
- mais pour autant, ils n'en gardent pas le même souvenir
- d'où l'importance de la narration, qui permet de montrer la perception de chaque personne de l'evenement
- une même scène pourrait du reste être jouer plusieurs fois, selon ce que les gens disent d'elle ex :
	- A  : tu te rappelle quand B est tombé à l'eau ? > on joue la scène ou B tombe à l'eau
	- B : pas du tout, je suis allé peche des truites et c'était une technique de peche > scène où B décide d'aller pecher des truites, puis où B est dans l'eau (ce qui pour A était "B tombe à l'eau")

