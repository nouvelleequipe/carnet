# Film petit buget

Concept vu des zannées en arrière au Zinema, joué par le PIP.

Rappelé du mieux que possible par *Camilo*.

## Abstract

Dans ce concept, le public vient voir le **tournage d'un film dont le budget fut dérobé**. La réalisatrice n'a plus rien à disposition, seulement des acteurs qu'elle à pu trouver et son synopsis: personnages et début d'histoire. 

Comme **première partie**, elle annonce les caractères de chacun des acteurs qui jouent l'un après l'autre une petite scène imposée. Ceci permet aux acteurs d'avoir une base de personnage. 

Pour la **partie principale**, la réalisatrice annonce la situation initiale du film, les personnages et les acteurs qui les joueront. Les acteurs improvisent le film à partir de ces éléments de base. De temps à autre, la réalisatrice demande de **préciser une scène particulière en cours ou de jouer une ellipse**.

## Format

Long form composé d'une partie d'introduction et du film, possible d'insérer une entracte dans la partie film. Avec un musicien improvisateur pour les émotions c'est idéal.

### Partie d'introduction des caractères

Le but est de présenter les caractères des personnages du film à venir et de les laisser jouer une petite scène dirigée

> Tu es une personne intelligente et capable, mais tu es timide au point que tes bonnes idées ne sont jamais écoutées, voire elles te sont volées. Quand vous devez-vous décider ou aller manger, tu propose le resto qu'il faut, tout le monde t'entend mais personne ne t'écoute, mais au final vous y allez quand même car un autre l'a proposé.
>
> On veut te voir dans une scène où ton manager t'annonce que ce n'est pas toi qui recoit la promotion mais le nouvel employé.

Chaque acteur aura sa description et sa scène à jouer (2 minutes). Puis la réalisatrice expose son film et les personnages.

### Exposition du film

Le but est de donner un petit point de départ : lieu, temps, voire enjeu initial et les personnages, ainsi que les acteurs qui les incarnent. Les personnages conservent les caractères présentés en introduction.

> C'est la période de Noël dans un village paumé dans une ville nordique. Les villageois se connaissent tous entre-eux et ils organisent le souper de noël ensemble. Il y a l'ingénieur du village, joué par [acteur plus haut], et sa voisine [autre acteur], dont il est secrètement amoureux. [autres personnages]. 
>
> La préparation du souper avance tant bien que mal, mais notre ingénieur découvre que le volcan d'à-côté va bientôt exploser....

Les acteurs se concertent et lancent la première scène du film.

### Interruptions de la réalisatrice

Le but est de laisser la réalisatrice diriger ses acteurs en leur demandant de jouer ou de continuer une scène précise.

> [les acteurs décident qu'ils ont pu négocier l'achat d'un traîneau et annoncent "Après la livraison du trâineau.." la séalisatrice COUPE.] J'aimerais bien voir la discussion entre l'ingénieur et le fournisseur de traineaux du village rival... [petit cocus et IMPRO]

